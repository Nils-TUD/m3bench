This is the benchmark repository for the ASPLOS'22 paper "M³s: Untrusted Cores in a Shared
System". The root repository contains scripts to execute the benchmarks, post process the results,
and generate the plots. The submodules `m3` and `bench-lx` contain M³s and Linux, respectively,
including their required infrastructure.

## Running the benchmarks

Use the following steps to run the benchmarks:

### 1. Clone the respository

```
git clone https://gitlab.com/Nils-TUD/m3bench.git --recursive
cd m3bench
```

### 2. Prepare all repositories

The preparation step will build the necessary infrastructure for M³s and Linux (cross compilers,
etc.). Just execute the `prepare.sh`:

```
./prepare.sh
```

### 3. Load the hardware platform onto the FPGA

The bitfiles for the hardware platform can be found in `m3/platform/hw/fpga_tools/bitfiles`.
Provided that Vivado Labs is installed, the following command can be used to load the latest bitfile
onto the FPGA:

```
( cd m3/platform/hw/fpga_tools/testcases/tc_rocket_boot && make program-fpga )
```

### 4. Run the benchmarks

This last step will first make sure that M³s and Linux themselves are up to date (i.e., before the
first run, M³s and Linux will be built) and run all benchmarks. The results including all log files
etc. are stored in the ``results'' directory. Note that benchmarks will be automatically repeated on
failure, because some occasional failures are unavoidable (e.g., sometimes loading programs onto the
FPGA fails due to UDP packet drops). Additionally, there are still some hardware/software bugs due
to system's complexity that we haven't found yet.

```
./run.sh
```

## Expected results

The raw data we obtained from our own run can be found in the directory `expected-results`. For
example, the following commands can be used to compare the results:

```
tail results/*.dat > res.txt
tail expected-results/*.dat > expected.txt
vimdiff res.txt expected.txt
```
