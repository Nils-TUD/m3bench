library(extrafont)
library(RColorBrewer)
source("tools/helper.R")

scaling <- 1.8
colors <- brewer.pal(n = 3, name = "Pastel1")

args <- commandArgs(trailingOnly = TRUE)

pdf(as.character(args[1]), width=10, height=2.2)
par(cex.lab=scaling, cex.axis=scaling, cex.main=scaling, cex.sub=scaling)

par(mar=c(4.5, 14, 1, 2))

vals = rev(scan(args[2]))
dev = rev(scan(args[3]))
zeros = rep(c(NA), 3)

barplot(zeros, xlim=c(0, 800), axes=F, space=rep(0.1, 3), horiz=T,
    names.arg=rep("", 3), xlab="Latency (µs)")
abline(v=c(seq(0, 800, 200)), col="gray80", lwd=2)

plot = barplot(
    vals,
    add=T,
    names=rev(c("M³v (isolated)", "M³v (shared)", "Linux")),
    space=rep(0.1, 3),
    horiz=T,
    xlim=c(0, 800),
    col=rev(colors),
    las=1,
)
error.bar(plot, vals, dev, horizontal=T)

dev.off()
embed_fonts(as.character(args[1]))
