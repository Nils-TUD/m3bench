#!/bin/sh

. tools/helper.sh

rscript_crop plots/asplos22-netlat/plot.R $1/eval-netlat.pdf $1/netlat-times.dat $1/netlat-stddev.dat
