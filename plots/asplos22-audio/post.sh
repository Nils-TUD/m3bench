#!/bin/sh

. tools/helper.sh

extract_m3_time() {
    grep "PERF" $1 | \
        sed -e 's/\x1b\[0m//g' | \
        sed -re 's/.*PERF "\S+.*": ([0-9]*).*/\1/'
}

extract_m3_stddev() {
    grep "PERF" $1 | \
        sed -e 's/\x1b\[0m//g' | \
        sed -re 's/.*PERF "\S+.*": [0-9]*.*- ([0-9.]*).*/\1/'
}

timeex=$(extract_m3_time $1/m3-voiceassist/output.txt)
timesh=$(extract_m3_time $1/m3-voiceassist-shared/output.txt)
echo $timeex $timesh > $1/audio-times.dat

timeex=$(extract_m3_stddev $1/m3-voiceassist/output.txt)
timesh=$(extract_m3_stddev $1/m3-voiceassist-shared/output.txt)
echo $timeex $timesh > $1/audio-stddev.dat
