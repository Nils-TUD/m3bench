#!/bin/sh

export LX_PLATFORM=hw
export LX_ARCH=riscv64

cd bench-lx

./b mkapps || exit 1
./b mklx || exit 1

run_bench() {
    name=$2
    outdir=$1/lx-$2
    mkdir -p $outdir

    while true; do
        /bin/echo -e "\e[1mStarting $2\e[0m"

        BENCH_CMD="/bench/bin/$2" ./b bench

        if [ "$(grep Benchmarks_done run/log.txt)" != "" ]; then
            /bin/echo -e "\e[1mFinished $2:\e[0m \e[1;32mSUCCESS\e[0m"
            break
        else
            /bin/echo -e "\e[1mFinished $2:\e[0m \e[1;31mFAILED\e[0m"
        fi
        sleep 1
    done

    cp run/{log,res}.txt $outdir
}

for b in syscall yield; do
    run_bench $1 $b
done
