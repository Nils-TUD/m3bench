#!/bin/sh

/bin/echo -e "\e[1mPreparing Linux...\e[0m"
cd bench-lx
export LX_ARCH=riscv64 LX_PLATFORM=hw
./b mkbr || exit 1
/bin/echo -e "\e[1mLinux is ready\e[0m"

/bin/echo -e "\e[1mPreparing M³...\e[0m"
cd ../m3/cross
./build.sh riscv || exit 1
/bin/echo -e "\e[1mM³ is ready\e[0m"

/bin/echo -e "\e[1mPreparing Rust...\e[0m"
rustup install nightly-2021-04-19
rustup default nightly-2021-04-19
rustup component add rust-src
/bin/echo -e "\e[1mRust is ready\e[0m"
