#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 <own IP>"
    exit 1
fi

root=$(readlink -f $(dirname $0))
ownip="$1"

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

( cd $root/audiorcv && cargo run --release udp $ownip 1337 1 ) &
( cd $root/latserv && cargo run --release $ownip 1338 ) &
( cd $root/ycsbclient && cargo run --release udp $ownip 1339 ) &

wait
