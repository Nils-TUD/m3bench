use std::vec::Vec;
use std::net::UdpSocket;

const VERBOSE: bool = false;

fn main() {
    let args = std::env::args().map(|a| a.to_string()).collect::<Vec<_>>();
    if args.len() != 3 {
        println!("Usage: {} <ip> <port>", args[0]);
        return;
    }

    let ep = format!("{}:{}", args[1], args[2]);
    println!("Listening for UDP packets on {}", ep);

    let socket = UdpSocket::bind(ep).expect("bind failed");

    let mut buffer = [0u8; 1024];
    loop {
        let (amt, src) = socket.recv_from(&mut buffer).expect("recv_from failed");
        if VERBOSE {
            println!("Received {} bytes from {}; echoing back", amt, src);
        }
        socket.send_to(&buffer[0..amt], src).expect("send_to failed");
    }
}
