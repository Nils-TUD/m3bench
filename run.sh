#!/bin/bash

export M3_HW_FPGA=1
export M3_HW_SSH=localhost
export LX_HW_SSH=localhost

lxbenchs=""
m3benchs=""
posts=""
plots=""

while [ $# -gt 0 ]; do
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: $0 [--linux <linux benchmarks>] [--m3 <m3 benchmarks>]"
        echo "      [--posts <post processings>] [--plots <plots>]"
        exit 1
    elif [ "$1" = "--linux" ]; then
        lxbenchs="$2"
    elif [ "$1" = "--m3" ]; then
        m3benchs="$2"
    elif [ "$1" = "--posts" ]; then
        posts="$2"
    elif [ "$1" = "--plots" ]; then
        plots="$2"
    else
        break
    fi
    shift 2
done

if [ "$lxbenchs" = "" ]; then
    lxbenchs+="asplos22-lx-fs asplos22-lx-micro asplos22-lx-netlat asplos22-lx-ycsb"
fi
if [ "$m3benchs" = "" ]; then
    m3benchs+="asplos22-m3-fs asplos22-m3-ipc asplos22-m3-netlat asplos22-m3-audio asplos22-m3-ycsb"
fi
if [ "$posts" = "" ]; then
    posts+="asplos22-fs asplos22-micro asplos22-netlat asplos22-audio asplos22-ycsb asplos22-codesize"
fi
if [ "$plots" = "" ]; then
    plots+="asplos22-fs asplos22-micro asplos22-ycsb"
fi

mkdir -p results
res=$(readlink -f results)

for b in $m3benchs; do
    /bin/echo -e "\e[1mStarting $b benchmark...\e[0m"
    ./benchs/$b.sh $res 1 || exit 1
done

for b in $lxbenchs; do
    /bin/echo -e "\e[1mStarting $b benchmark...\e[0m"
    ./benchs/$b.sh $res 1 || exit 1
done

for p in $posts; do
    /bin/echo -e "\e[1mStarting $p post processing...\e[0m"
    ./plots/$p/post.sh $res || exit 1
done

for p in $plots; do
    /bin/echo -e "\e[1mStarting $p plot...\e[0m"
    ./plots/$p/plot.sh $res || exit 1
done
